@extends('layout.app')

@section('title','Reset Password')
    
@section('content')
<div class="card card-primary">
        <div class="card-header"><h4>Reset Password</h4></div>

        <div class="card-body">
          <p class="text-muted">Silahkan Ganti password anda</p>
          <form method="POST" action="{{ route('password.update') }}">
            @csrf
            <input type="hidden" name="token" value="{{ $token }}">
            <div class="form-group">
              <label for="email">Email</label>
              <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}" tabindex="1" required autofocus>
                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
              <label for="password">New Password</label>
              <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} pwstrength" data-indicator="pwindicator" name="password" tabindex="2" required>
              <div id="pwindicator" class="pwindicator">
                <div class="bar"></div>
                <div class="label"></div>
              </div>
              @if ($errors->has('password'))
                 <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('password') }}</strong>
                 </span>
              @endif
            </div>

            <div class="form-group">
              <label for="password-confirm">Confirm Password</label>
              <input id="password-confirm" type="password" class="form-control" name="password_confirmation" tabindex="2" required>
            </div>

            <div class="form-group">
              <button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4">
                Reset Password
              </button>
            </div>
          </form>
        </div>
      </div>
@endsection