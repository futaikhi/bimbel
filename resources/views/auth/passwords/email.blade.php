@extends('layout.app')

@section('title','Forgot Password')

@section('content')
            <div class="card card-primary">
              <div class="card-header"><h4>Forgot Password</h4></div>

              <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        Cek email anda untuk mengganti password
                    </div>
                @endif
                <p class="text-muted">Kami akan mengirimkan link untuk mengganti password</p>
                <form method="POST" action="{{ route('password.email') }}">
                    @csrf
                  <div class="form-group">
                    <label for="email">Email</label>
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" tabindex="1" required autofocus>
                    @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                </div>

                  <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4">
                      Forgot Password
                    </button>
                  </div>
                </form>
              </div>
            </div>
@endsection