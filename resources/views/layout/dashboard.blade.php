<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Dashboard</title>

  <!-- General CSS Files -->
<link rel="stylesheet" href="{{ URL::asset('modules/bootstrap/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{ URL::asset('modules/fontawesome/css/all.min.css')}}">

  <!-- CSS Libraries -->
<link rel="stylesheet" href="{{ URL::asset('modules/bootstrap-social/bootstrap-social.css')}}">

  <!-- Template CSS -->
<link rel="stylesheet" href="{{ URL::asset('css/style.css')}}">
<link rel="stylesheet" href="{{ URL::asset('css/components.css')}}">

  <!-- Icon -->
<link rel="icon" href="{{ URL::asset('img/logo.png')}}">
<!-- Start GA -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-94034622-3"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-94034622-3');
</script>
<!-- /END GA --></head>

<body>
    <div id="app">
        <div class="main-wrapper main-wrapper-1">
          <div class="navbar-bg"></div>
          <nav class="navbar navbar-expand-lg main-navbar">
             <form class="form-inline mr-auto">
                 <ul class="navbar-nav mr-3">
                    <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
               </ul>
           </form>
           <ul class="navbar-nav navbar-right">
              <li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
                <img alt="image" src="{{ auth()->user()->avatar }}" class="rounded-circle mr-1">
                <div class="d-sm-none d-lg-inline-block"> {{ auth()->user()->name }}</div></a>
                <div class="dropdown-menu dropdown-menu-right">
                  <a href="{{ route('logout') }}" class="dropdown-item has-icon text-danger"  
                  onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();">
                    <i class="fas fa-sign-out-alt"></i> Logout
                  </a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf
                  </form>
                </div>
              </li>
            </ul>
          </nav>
          <div class="main-sidebar sidebar-style-2">
            <aside id="sidebar-wrapper">
              <div class="sidebar-brand">
                <a href="{{ route('awal')}}">Bimbel</a>
              </div>
              <div class="sidebar-brand sidebar-brand-sm">
                <a href="{{ route('awal')}}">B</a>
              </div>
              <ul class="sidebar-menu">
                    <li class="menu-header">Dashboard</li>
                    <li class="dropdown">
                      <a href="#" class="nav-link has-dropdown"><i class="fas fa-fire"></i><span>Dashboard</span></a>
                      <ul class="dropdown-menu">
                        <li><a class="nav-link" href="#">General Dashboard</a></li>
                        <li><a class="nav-link" href="#">Ecommerce Dashboard</a></li>
                        <li><a class="nav-link" href="#">Ecommerce Dashboard</a></li>
                        <li><a class="nav-link" href="#">Ecommerce Dashboard</a></li>
                      </ul>
                    </li>
              </ul>
          </div>
    
          <!-- Main Content -->
          <div class="main-content">
            <section class="section">
              <div class="section-header">
                <h1>@yield('title')</h1>
              </div>
    
              <div class="section-body">
                @yield('isi')
              </div>
            </section>
          </div>
          <footer class="main-footer">
            <div class="footer-left">
              Copyright &copy; 2018 <div class="bullet"></div> Design By <a href="https://nauval.in/">Muhamad Nauval Azhar</a>
            </div>
            <div class="footer-right">
              
            </div>
          </footer>
        </div>
      </div>

  <!-- General JS Scripts -->
<script src="{{ URL::asset('modules/jquery.min.js')}}"></script>
<script src="{{ URL::asset('modules/popper.js')}}"></script>
<script src="{{ URL::asset('modules/tooltip.js')}}"></script>
<script src="{{ URL::asset('modules/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{ URL::asset('modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
<script src="{{ URL::asset('modules/moment.min.js')}}"></script>
<script src="{{ URL::asset('js/stisla.js')}}"></script>
  
  <!-- JS Libraies -->

  <!-- Page Specific JS File -->
  
  <!-- Template JS File -->
<script src="{{ URL::asset('js/scripts.js')}}"></script>
<script src="{{ URL::asset('js/custom.js')}}"></script>
</body>
</html>