<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>@yield('title') Bimbel</title>

  <!-- General CSS Files -->
<link rel="stylesheet" href="{{ URL::asset('modules/bootstrap/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{ URL::asset('modules/fontawesome/css/all.min.css')}}">

  <!-- CSS Libraries -->
<link rel="stylesheet" href="{{ URL::asset('modules/bootstrap-social/bootstrap-social.css')}}">

  <!-- Template CSS -->
<link rel="stylesheet" href="{{ URL::asset('css/style.css')}}">
<link rel="stylesheet" href="{{ URL::asset('css/components.css')}}">

  <!-- Icon -->
<link rel="icon" href="{{ URL::asset('img/logo.png')}}">
<!-- Start GA -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-94034622-3"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-94034622-3');
</script>
<!-- /END GA --></head>

<body>
    <div id="app">
        <section class="section">
          <div class="container mt-5">
            <div class="row">
              <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-8 offset-lg-2 col-xl-8 offset-xl-2">
                <div class="login-brand">
                <a href="{{ route('awal') }}"><img src="{{ URL::asset('img/logo.png')}}" alt="logo" width="100" class="shadow-light rounded-circle"></a>
                </div>
    @yield('content')
    <div class="simple-footer">
        Copyright &copy; Stisla 2018
      </div>
    </div>
  </div>
</div>
</section>
</div>

  <!-- General JS Scripts -->
<script src="{{ URL::asset('modules/jquery.min.js')}}"></script>
<script src="{{ URL::asset('modules/popper.js')}}"></script>
<script src="{{ URL::asset('modules/tooltip.js')}}"></script>
<script src="{{ URL::asset('modules/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{ URL::asset('modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
<script src="{{ URL::asset('modules/moment.min.js')}}"></script>
<script src="{{ URL::asset('js/stisla.js')}}"></script>
  
<script src="{{ URL::asset('modules/jquery-pwstrength/jquery.pwstrength.min.js')}}"></script>
<script src="{{ URL::asset('modules/jquery-selectric/jquery.selectric.min.js')}}"></script>

<!-- Page Specific JS File -->
<script src="{{ URL::asset('js/page/auth-register.js')}}"></script>

  
  <!-- Template JS File -->
<script src="{{ URL::asset('js/scripts.js')}}"></script>
<script src="{{ URL::asset('js/custom.js')}}"></script>
</body>
</html>