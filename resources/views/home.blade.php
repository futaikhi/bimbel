@extends('layout.dashboard')

@section('title','PROFIL')

@section('isi')
    <div class="" align = "center">
            <img src="{{ auth()->user()->avatar }}" class="img-thumbnail d-block mx-auto" alt="">
    </div>
    <div class="card" align = "center">
        <div class="card-body">
                <div class="table-responsive">
                        <table class="table w-100">
                          <tbody>
                            <tr>
                              <td>
                                <b>Nama</b>
                              </td>
                              <td>{{ auth()->user()->name }}</td>
                            </tr>
                            <tr>
                              <td>
                                <b>Email</b>
                              </td>
                              <td>{{ auth()->user()->email }}</td>
                            </tr>
                            <tr>
                                <td>
                                  <b>Facebook</b>
                                </td>
                                @if (auth()->user()->facebook_id != 0)
                                <td>Connect</td>
                                @else
                                <td>Tidak terhubung dengan Facebook</td>
                                @endif
                            </tr>
                            <tr>
                                <td>
                                  <b>Google</b>
                                </td>
                                @if (auth()->user()->google_id != 0)
                                <td>Connect</td>
                                @else
                                <td>Tidak tehubung dengan Google</td>
                                @endif
                             </tr>
                          </tbody>
                        </table>
                      </div>
        </div>
    </div>
@endsection