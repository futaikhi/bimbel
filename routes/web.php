<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('awal');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/redirect/google', 'Auth\LoginController@redirectToGoogle')->name('login.google');
Route::get('/google/callback', 'Auth\LoginController@handleGoogleCallback');
Route::get('/redirect/facebook', 'Auth\LoginController@redirectToFacebook')->name('login.facebook');
Route::get('/facebook/callback', 'Auth\LoginController@handleFacebookCallback');

