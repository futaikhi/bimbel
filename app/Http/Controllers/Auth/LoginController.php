<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\Mail\Confirm;
use Illuminate\Support\Facades\Mail;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function redirectToGoogle(){
        return Socialite::driver('google')->redirect();
    }

    public function handleGoogleCallback(){
        $user = Socialite::driver('google')->user();

        // check if they're an existing user
        $strRandom = str_random(8);
        $hashed_random_password = Hash::make($strRandom);
        $existingUser = User::where('email', $user->email)->first();
        if($existingUser){
            // log them in
            $checkGoogle = User::where('google_id', $user->id)->first();
            if($checkGoogle){
                auth()->login($checkGoogle, true);
            }
            else {
                $update = User::where('email',$user->email)
                ->update(['google_id' => $user->id]);
                $sukses = User::where('google_id',$user->id)->first();
                auth()->login($sukses, true);
            }
        } else {
            $newUser                  = new User;
            $newUser->name            = $user->name;
            $newUser->email           = $user->email;
            $newUser->google_id       = $user->id;
            $newUser->facebook_id     = '0';
            $newUser->avatar          = $user->avatar;
            $newUser->password        = $hashed_random_password;
            $newUser->avatar_original = $user->avatar_original;
            $newUser->save();
            auth()->login($newUser, true);

            Mail::to($user->email)->send(new Confirm($user->name,$user->email,$strRandom));
            
        }
        return redirect()->to('/home');
    }

    public function redirectToFacebook(){
        return Socialite::driver('facebook')->redirect();
    }

    public function handleFacebookCallback(){
        $user = Socialite::driver('facebook')->user();

        // check if they're an existing user
        $strRandom = str_random(8);
        $hashed_random_password = Hash::make($strRandom);
        $existingUser = User::where('email', $user->email)->first();
        if($existingUser){
            // log them in
            $checkFacebook = User::where('facebook_id', $user->id)->first();
            if($checkFacebook){
                auth()->login($checkFacebook, true);
            }
            else {
                $update = User::where('email',$user->email)
                ->update(['facebook_id' => $user->id]);
                $sukses = User::where('facebook_id',$user->id)->first();
                auth()->login($sukses, true);
            }
        } else {
            $newUser                  = new User;
            $newUser->name            = $user->name;
            $newUser->email           = $user->email;
            $newUser->facebook_id     = $user->id;
            $newUser->google_id       = '0';
            $newUser->avatar          = $user->avatar;
            $newUser->avatar_original = $user->avatar_original;
            $newUser->save();
            auth()->login($newUser, true);

            Mail::to($user->email)->send(new Confirm($user->name,$user->email,$strRandom));
        }
        return redirect()->to('/home');
    }
}
